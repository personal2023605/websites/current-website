# VSCode

- `alt+down` or `alt+up`
    - Moves current line down or up
- `alt+shift+down` or `alt+shift+up`
    - Copy current line down or up
- `alt+shift+left` or `alt+shift+right`
    - Select next text enclosure
    - Ex. in code `{ test: 'hello' }` if cursor is in 'hello', this will first select `'hello'` then it will select the whole thing
- `ctrl+enter`
    - Start new line and jump to it
    - Ex. if cursor is at the middle of a line, this will start a new line and not bring anything after the cursor with it
- `ctrl+p`
    - Search for file in workspace
- `ctrl+k s`
    - Save all open files
- `ctrl+k w`
    - Close all open files in view
- `ctrl+k ctrl+i`
    - Works the same as hovering over the text
- `ctrl+.`
    - Pull up code actions (fix errors, etc.)
- `F8`
    - Go to next error in file
- `pg up` or `pg down`
    - Scroll the page faster
- `ctrl+k ctrl+s`
    - Pull up shortcuts window
- `ctrl+,`
    - Pull up settings
- `ctrl+shift+p`
    - Run vscode command
- `ctrl+k i`
    - This is a custom keybinding, need to set this manually
        - Name: closeOtherEditors
    - Close other tabs

## Side Panel
- `ctrl+b`
    - Toggle panel open and closed
- Debugging
    - `ctrl+shift+d`
        - Open debugger panel
    - `F9`
        - Places breakpoint on current line
    - `F10`
        - Step over
    - `F11`
        - Step into
    - `F5`
        - Start debugger
    - `shift+F5`
        - Stop debugger
- Searching
    - `ctrl+shift+f`
        - Search for all
    - `F4` or `shift+F4`
        - Go to next search result

## Usefulness Rank
1. `ctrl+p`
2. `ctrl+shift+f`
3. `alt+shift+down` or `alt+shift+up`
4. `alt+down` or `alt+up`
5. `ctrl+enter`
6.

## Most Useful Extensions
1. GitHub CoPilot
2.
