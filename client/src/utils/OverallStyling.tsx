import { createGlobalStyle } from 'styled-components'

import { COLORS, STYLES } from './Constants'

export default createGlobalStyle`
    body {
        --sb-track-color: #0000000;
        --sb-thumb-color: #9d9d9d7f;
        --sb-size: 6px;
    }

    *::-webkit-scrollbar {
        width: var(--sb-size);
    }

    *::-webkit-scrollbar-track {
        background: var(--sb-track-color);
        border-radius: 3px;
    }

    *::-webkit-scrollbar-thumb {
        background: var(--sb-thumb-color);
        border-radius: 3px;
    }

    @supports not selector(::-webkit-scrollbar) {
        * {
            scrollbar-color: var(--sb-thumb-color) var(--sb-track-color);
        }
    }



    #root {
        overflow: auto;
    }

    body {
        background-color: ${COLORS.BACKGROUND};
        color: ${COLORS.TEXT};
        ${STYLES.BODY('&')}
    }

    .paragraph {
        margin: 0;
    }

    a {
        text-decoration: none;
        color: ${COLORS.TEXT};
        transition: color 0.5s;
    }

    a:hover { color: ${COLORS.HOVER_TEXT}; }

    .page {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .title {
        align-self: center;
        
        ${STYLES.TITLE('&')}
    }

    .label {
        font-size: 1rem;
    }

    .text {
        line-height: normal;
        margin: 0;
        font-size: 1rem;
    }

    .cardTitle {
        margin: 0;
        ${STYLES.CARD_TITLE('&')}
    }

    p {
        line-height: normal;
        margin: 0;
    }
`
