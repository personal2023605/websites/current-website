function subscribe(eventName: string, listener: () => void) {
    document.addEventListener(eventName, listener)
}

function unsubscribe(eventName: string, listener: () => void) {
    document.removeEventListener(eventName, listener)
}

function publish(eventName: string, data?: string) {
    const event = data ? new CustomEvent(eventName, {detail: data}) : new CustomEvent(eventName)
    document.dispatchEvent(event)
}

export { publish, subscribe, unsubscribe }