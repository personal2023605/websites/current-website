import Constants from './Constants'
// import debounce from 'lodash.debounce'

import React, { useEffect, useLayoutEffect, useState } from 'react'


/**
 * Returns the current window size
 */
const useWindowSize = (): {width: number, height: number} => {
    const [size, setSize] = useState({ width: window.innerWidth, height: window.innerHeight })
    const updateSize = () => {
        setSize({ width: window.innerWidth, height: window.innerHeight })
    }
    useLayoutEffect(() => {        
        window.addEventListener('resize', updateSize)
        return () => window.removeEventListener('resize', updateSize)
    }, [])
    return size
}

/**
 * Returns if the current device is a mobile device
 */
export const isMobileDevice = (): boolean => {
    return useWindowSize().width < Constants.MOBILE
}

export const setVariableCssVars = (): void => {
    useEffect(() => {
        // const setVh = debounce(() => document.documentElement.style.setProperty('--vh', `${window.innerWidth}px`), 10)
        const setVh = () => {
            document.documentElement.style.setProperty('--vh', `${window.innerWidth}px`)
        }

        window.addEventListener('resize', setVh)

        return () => {
            window.removeEventListener('resize', setVh)
        }
    })
}

export const SetDimensions = (): React.ReactElement => {
    // This is a new component because when in <App>, it was reloading if scrolled in too soon and looking bad.
    document.documentElement.style.setProperty('--vh', `${window.innerWidth}px`)
    console.log(`Width: ${window.innerWidth}`)
    setVariableCssVars()
    return (<></>)
}
