import { css } from 'styled-components'

export default class {
    static MOBILE = 500 // Size cutoff for mobile devices in pixels
}

export class COLORS {
    // Primary colors
    static PRIMARY = '#445473'
    static BACKGROUND = '#181818'
    static CARD_BACKGROUND = '#1f1f1f'
    static FORM_BACKGROUND = '#000000'
    static TEXT = '#FFFFFF'
    static HOVER_TEXT = '#9d9d9d'
    static TRANSPARENT_BACKGROUND = '#171717b6'
    
    // Dark mode colors
    static DARK_TEXT = '#FFFFFF'
    static DARK_HOVER_TEXT = '#ababab'
    static DARK_DISABLED_TEXT = '#8989896c'
    
    // Button colors
    static BTN_BG = '#D7E4C0'
    static BTN_BG_HOVER = '#b6bfa6'
    static BTN_TEXT = '#282828'
    static BTN_HOVER_TEXT = this.BTN_TEXT
    static BTN_BORDER = '#00000000' // Transparent
}

// TODO: Change these so they can be indexed by the page name
export const PROJECT_PAGES = [
    { index: 3, name: 'senior design', path: '/projects/seniorDesign', subpath: true },
    { index: 4, name: 'capstone', path: '/projects/capstone', subpath: true },
    { index: 5, name: 'personal website', path: '/projects/personalWebsite', subpath: true },
    { index: 6, name: 'home server', path: '/projects/homeServer' , subpath: true },
    { index: 7, name: 'personal robot', path: '/projects/personalRobot', subpath: true }
]

export const PAGES = [
    { index: 0, name: 'home', path: '/home' },
    { index: 1, name: 'resume', path: '/resume' },
    { index: 2, name: 'projects', path: '/projects' },
    ...PROJECT_PAGES
]

export class STYLES {
    static readonly PAGE_HEIGHT = '94vh'

    static readonly TITLE = (selector: string) => {
        return css`
            ${selector} {
                margin: 0;
                font-size: calc(var(--vh) * .06);
                letter-spacing: 1px;
                line-height: 1.1;
                padding: 5% 0;
            }
        `
    }

    static readonly SUBTITLE = (selector: string) => {
        return css`
            ${selector} {
                margin: 0;
                font-size: calc(var(--vh) * .06);
                letter-spacing: 1px;
            }
        `
    }

    static readonly CARD_TITLE = (selector: string) => {
        return css`
            ${selector} {
                margin: 0;
                font-size: min(calc(var(--vh) * .04), 24px);
            }
        `
    }

    static readonly BODY = (selector: string) => {
        return css`
            ${selector} {
                line-height: 1.5;
                margin: 0;
                font-size: 14px;
            }
        `
    }
}
