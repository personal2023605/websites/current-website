import Home from 'pages/home/Home'
import { PAGES } from 'utils/Constants'
import ProjectPage from 'pages/projects/ProjectPage'
import Projects from 'pages/projects/Projects'
import React from 'react'
import Resume from 'pages/Resume'
import { toTitleCase } from './HelpfulFunctions'

import { AnimatePresence, motion } from 'framer-motion'

const addFramer = (element: JSX.Element, path: string) => {
    return (
        <AnimatePresence mode='wait'>
            <motion.div style={{outline: 'none', display: 'flex', flexDirection: 'column'}} id={'pageContainer'}
                key={path}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{
                    duration: 0.8
                }}>
                {element}
            </motion.div>
        </AnimatePresence>
    )
}

export const ROUTE_MAP = [
    { path: '/', element: addFramer(<Home/>, '/')},
    { path: PAGES[0].path, element: addFramer(<Home/>, PAGES[0].path)},
    { path: PAGES[1].path, element: addFramer(<Resume/>, PAGES[1].path)},
    { path: PAGES[2].path, element: addFramer(<Projects/>, PAGES[2].path)},
    { path: PAGES[3].path, element: addFramer(<ProjectPage name={toTitleCase(PAGES[3].name)}/>, PAGES[3].path)},
    { path: PAGES[4].path, element: addFramer(<ProjectPage name={toTitleCase(PAGES[4].name)}/>, PAGES[4].path)},
    { path: PAGES[5].path, element: addFramer(<ProjectPage name={toTitleCase(PAGES[5].name)}/>, PAGES[5].path)},
    { path: PAGES[6].path, element: addFramer(<ProjectPage name={toTitleCase(PAGES[6].name)}/>, PAGES[6].path)},
    { path: PAGES[7].path, element: addFramer(<ProjectPage name={toTitleCase(PAGES[7].name)}/>, PAGES[7].path)}
]