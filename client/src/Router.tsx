import { CgMenu } from 'react-icons/cg'
import { COLORS } from 'utils/Constants'
import { ROUTE_MAP } from './utils/RoutingConstants'
import SearchOverlay from './components/SearchOverlay'
import styled from 'styled-components'

import { createBrowserRouter, Outlet, useOutletContext } from 'react-router-dom'
import { publish, subscribe, unsubscribe } from './utils/Events'
import React, { useEffect, useState } from 'react'

const AppContainer = styled.div`
    height: 100%;

    & > #outletContainer {
        grid-row: 1;
        height: 100%;
    }
`

const Hamburger = styled.div`
    font-size: unset !important;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 100;
    margin: 2%;
    color: ${COLORS.BTN_BG};
    transition: color 0.5s;

    &:hover {
        cursor: pointer;
        color: ${COLORS.BTN_BG_HOVER};
    }
`

const Layout = () => {
    const [showOverlay, setOverlay] = useState<boolean>(false)

    // eslint-disable-next-line complexity
    const _keyPressHandler = (event: KeyboardEvent) => {
        const modifierKeyPressed = event.metaKey || event.shiftKey
        if (!showOverlay && process.env.REACT_APP_ENV === 'development' && event.key.toLowerCase() === 'o' && !modifierKeyPressed) {
            publish('openOverlay')
        }
        else if (!showOverlay && process.env.REACT_APP_ENV !== 'development' && !modifierKeyPressed) {
            publish('openOverlay')
        }
        else if (event.key.toLowerCase() === 'escape') {
            publish('closeOverlay')
        }
    }

    useEffect(() => {
        subscribe('openOverlay', () => setOverlay(true))
        subscribe('closeOverlay', () => setOverlay(false))
        document.addEventListener('keydown', _keyPressHandler)
        return () => {
            unsubscribe('openOverlay', () => setOverlay(true))
            unsubscribe('closeOverlay', () => setOverlay(false))
            document.removeEventListener('keydown', _keyPressHandler)
        }
    })

    return (
        <AppContainer>
            {/* TODO: Disappear when showOverlay is on */}
            <Hamburger onClick={() => publish(showOverlay ? 'closeOverlay' : 'openOverlay')}><CgMenu size={50}/></Hamburger>
            <div style={{outline: 'none'}} id={'outletContainer'}>
                <Outlet context={showOverlay satisfies boolean} />
            </div>
            <SearchOverlay visible={showOverlay} />
        </AppContainer>
    )
}

export default createBrowserRouter([
    {
        path: '/',
        element: <Layout/>,
        children: ROUTE_MAP
    }
])

export function useOverlay() {
    return useOutletContext<boolean>()
}