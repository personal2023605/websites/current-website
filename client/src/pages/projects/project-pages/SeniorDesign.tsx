import React from 'react'
import zedCameraMap from '../markdown-assets/senior-design/zed-camera-map.png'
import pcbDesign from '../markdown-assets/senior-design/pcb-design.png'

const SeniorDesign = () => {
    return (
        <>
            <div className='subtitle'>Summary</div>
            <div className='section-description'>
                The premise of this project was to finish a robot that the Temple Robotics team had begun constructing. The robot was originally designed by JPL as a Mars rover miniature prototype. The goal of the project was to improve the design of the robot and begin the autonomous navigation. There were two main aspects that needed improvement: the robot&apos;s PCB and its autonomous navigation.

                The robot was designed to have ten motors total for the drive train. These motors were set up in a rocker bogey system with six motors controlling the wheels and four controlling the corner legs for turning. This design allowed the robot to navigate rough terrain more effectively and maintain stability while traversing obstacles. The rocker bogey mechanism ensured that all wheels remained in contact with the ground, distributing the weight evenly and providing better traction.
            </div>

            <div className='subtitle'>PCB Design</div>
            <div className='section-description'>
                For the autonomous capabilities to be tested, the robot needed to be in a functional state. For this, the PCB needed to be designed and manufactured. The PCB was designed in KiCAD and manufactured by JLCPCB. Originally, the robot was equipped with a PCB designed by JPL. Although the circuit board was functional for the original design, the robotics team decided that it wanted to increase the motor speed. While doing so, they did not account for the fact that the motors would draw more current, though, and this ended up being a predicament when the robot was tested.

                Our goal was to create a circuit board which would be able to handle the increased current draw from the motors. To achieve this, we redesigned the PCB to include thicker traces and higher current-rated components. Additionally, we added more robust power management features to ensure the stability and reliability of the robot&apos;s operation under increased load conditions. A schematic of the PCB can be seen below:

                <div className='description-img-container'>
                    <img style={{width: '30%', height: '30%'}} src={pcbDesign}></img>
                    <div className='img-caption'>PCB schematic with widened traces</div>
                </div>
            </div>

            <div className='subtitle'>3D Modeling</div>
            <div className='section-description'>
                To conform the robot for autonomous capabilities, some 3D design was required for mounting and protecting the sensors. The robot was equipped with a stereo camera for mapping the environment and the motors with encoders for odometry. The mount for the stereo camera was designed to elevate the camera to a height that would allow for a better view of the environment. This is shown in the image below:

                {/* TODO: Insert image of the stereo camera mount */}

                The head of the robot was also designed to be 3D printed. This designed housed an arduino for controlling the LCD panel and wiring for this. It was also servo controlled to allow for panning of the panel. The design is shown below:

                {/* TODO: Insert an image of the LCD panel head */}
            </div>

            <div className='subtitle'>Microcomputers</div>
            <div className='section-description'>
                The brain of the system was a Jetson Nano. The Nano was in control of the autonomous and processing of navigation data and would directly receive vision data from the stereo camera and encoder data from the motors. The Nano would communicate with an Arduino over serial, which would control the speed and direction of the motors while processing encoder data to pass to the Nano. 

                For control of the motors, sabertooth motor controllers were utilized with kangaroo motion controllers for reading encoder values. At first, RoboClaw motor controllers were tested but these proved to be too difficult to set up and tune for encoder purposes. The switch to Sabertooth&apos;s eventually made the control system more reliable.
            </div>

            <div className='subtitle'>Programming</div>
            <div className='section-description'>
                The programming was mainly written in Python using ROS for messaging. There was also some Arduino C programming for simple motor control. A large launch file was also created when utilizing the ZED stereo camera to set the parameters for the camera.

                The rosserial_python ROS package was used for communication between the Jetson Nano and the Arduino. This would allow a USB cord to send messages to the Arduino through publishing to a topic on the Nano and subscribing to the same topic in the arduino.

                Two custom ROS messages were created to send motor speeds and direction over two topics to the Arduino from the Nano. One message was for the robot wheel speeds, and another was for positioning for the motors controlling the turning radius.
            </div>

            <div className='subtitle'>Stereo Camera and Environment Mapping</div>
            <div className='section-description'>
                A ZED stereo camera was used for the mapping of the environment due to its availability to the club. Stereo cameras use two lens to triangulate and compare images which can determine the distance of an object. We would have preferred to use a LiDAR sensor for the mapping, but these proved to be too expensive to acquire and therefore the team had to resort to using a stereo camera instead.

                The robot would map out its environment, moving slowly while it does so. This process would happen similarly to a Rumba, where it would first create a map which would eventually allow it to navigate freely throughout the environment. An example image of a map created by our robot can be seen below:

                <div className='description-img-container'>
                    <img src={zedCameraMap}></img>
                    <div className='img-caption'>The 3D map created with the ZED camera</div>
                </div>
            </div>
        </>
    )
}

export default SeniorDesign