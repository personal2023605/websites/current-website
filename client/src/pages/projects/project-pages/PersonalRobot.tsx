import React from 'react'

const PersonalRobot = () => {
    return (
        <div className='project-container'>
            <div className='project-section'>
                <div className='subtitle'>Summary</div>
                <div className='section-description'>
                    For this project, I wanted to have a robot that can be controlled over a website.
                    I also wanted the video stream of the website to display on the home page. One requirement
                    of the website was that it had to have a password system so that only certain people can
                    access it. Otherwise random people could see the video stream and essentially spy on me.
                    Another desired requirement is that the robot uses ROS nodes for its scripts since this
                    will make it more modular. The software stack of the robot includes the following:

                    <ul>
                        <li>ROS</li>
                        <li>React</li>
                        <li>TypeScript</li>
                        <li>PostgreSQL</li>
                        <li>ExpressJS</li>
                        <li>Nginx</li>
                        <li>Docker</li>
                        <li>Cloudflare</li>
                    </ul>

                    These softwares are all used to program and deploy the website and robot.
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>Wiring and Hardware</div>
                <div className='section-description'>
                    Before doing any programming, I needed to confirm that the basic hardware of the robot
                    was functioning properly. This is a list of the hardware being used for the bot:

                    <ul>
                        <li>1300 mAh LiPo battery</li>
                        <li>LM2596 DC to DC Buck Converter</li>
                        <li>Raspberry Pi 4 with Ubuntu 20.04 32 bit architecture</li>
                        <li>Raspberry Pi Camera Board v1.3</li>
                        <li>L298N Dual H-bridge Motor Controller</li>
                        <li>200RPM DC 3-6V Gearbox Motor</li>
                        <li>USB WiFi Adapter for Raspberry Pi (for better connection)</li>
                    </ul>

                    I ran into a couple issues when trying to set up all the hardware for this. First of all,
                    apparently the Raspberry Pi Camera only works with a 32 bit OS. When using the 64 bit
                    Ubuntu 20.04 server, the camera feed would not be listed in the /dev/video0 spot as it
                    should be. This was fixed by switching over to the Ubuntu 20.04 32 bit image and appending 
                    <code> start_x=1 </code> to the <code> /boot/firmware/config.txt </code> file.
                    <br/><br/>
                    I also ran into another problem that occurred because the raspberry pi did not have the a
                    common ground with the battery. When the battery was powering the motor controller, and the
                    wall was powering the raspberry pi, they did not share a common ground and therefore the
                    messages being sent from the raspberry pi to the motor controller were not read correctly.
                    Adding an extra wire connecting the ground of the battery to a ground GPIO raspberry pi
                    pin helped solve this.
                    <br/><br/>
                    Once these issues were sorted out, the rest of the hardware was finished quickly and work
                    was able to be started on the programming of the bot. The high-level wiring diagram can be
                    seen below:
                    <br/>
                    {/* TODO: Insert wiring diagram for the robot with caption */}
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>ROS Programming</div>
                <div className='section-description'>
                    To set up ROS, I needed to confirm a few things first. First of all, the hardware of the robot
                    needed to be finished so that the programming could be tested. Secondly, I wanted to test the
                    robot with a bluetooth controller so I needed to set up bluetooth on the raspberry pi.

                    For bluetooth, bluetoothctl was used. This allowed me to connect a DualShock4 directly to the
                    raspberry pi. The following instructions are what I used to pair the controller.
                    
                    <blockquote>
                        <code>
                            scan on<br/>
                            
                        </code>
                    </blockquote>
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>Website</div>
                <div className='section-description'>
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>WebSocket and Video Messaging</div>
                <div className='section-description'>
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>PostgreSQL</div>
                <div className='section-description'>
                </div>
            </div>

            <div className='project-section'>
                <div className='subtitle'>Iterations</div>
                <div className='section-description'>
                    I have gone through many different iterations in modeling, wiring, and component usage.

                    My first design was simply a proof of concept. I wanted to create a robot which would move through commands on a website and would display a camera feed on the same website. In this part of the project, I only used a raspberry pi, battery, 5V regulator, motor controller, four cheap motors, and raspberry pi camera. The main difficulty for this iteration was implementing the website code to communicate with the robot using ROS nodes. After completion of this, the robot was able to display a video feed to and be controlled from a website.

                    The next iteration, I wanted to improve the design by implementing a system that can 
                </div>
            </div>
        </div>
    )
}

export default PersonalRobot