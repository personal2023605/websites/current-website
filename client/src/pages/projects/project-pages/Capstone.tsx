import React from 'react'
import capstoneUi from '../markdown-assets/capstone/capstone-ui.png'

const SeniorDesign = () => {
    return (
        <>
            <div className='subtitle'>Summary</div>
            <div className='section-description'>
                This was my capstone project for my computer science bachelor degree. The premise of the project was to take an already constructed robot kit and alter the code
                for it to make the robot more interesting and partially autonomous. 
                In order to do this, I created a website on my server at my apartment which the robot would connect to and send a video stream to and receive commands from.
            </div>

            <div className='subtitle'>Robot Design</div>
            <div className='section-description'>
                The design and tutorials for the robot can be found through <a href='https://www.adeept.com/learn/detail-34.html'>this link</a>. There was one alteration that had to be done to the robot for our use case, which was 3D model a camera mount which would point the camera down towards the ground. This would allow for the camera to see the lines on the ground which would resemble road lines, and drive along them. 
            </div>

            <div className='subtitle'>Front End UI</div>
            <div style={{padding: '0'}} className='section-description'>
                The front end of the website is coded using React. It is an interface with some buttons as pictured below. When certain keys are pressed, the robot will respond accordingly.

                <div className='description-img-container'>
                    <img style={{width: '60%', height: '60%'}} src={capstoneUi}></img>
                    <div className='img-caption'>The UI of the website without the video stream</div>
                </div>
            </div>

            <div className='subtitle'>Back End JavaScript</div>
            <div className='section-description'>
                The back end of the website is where all the magic happens. The back end is coded using Node.js and Express. The back end is responsible for sending and receiving commands to the robot, and also for sending and receiving images from the robot. The back end also sends the images to the front end for display. It communicates with the robot using WebSockets with the video stream implementing gstreamer.
            </div>

            <div className='subtitle'>Robot Code</div>
            <div className='section-description'>
                The robot code was originally given to me in Python, so I decided to write the changes to the code in python as well for modularity and simplicity. Two threads were added to the robot code to allow for images to be sent and commands to be received asynchronously.

                The first thread is for sending images to the server for processing. This is simply just a gstreamer pipe that sends the images directly, nothing else is done in this thread so that the robot code runs quickly.

                The second thread is more advanced and handles all the commands being sent to the robot and also sends out ultrasonic distance data for autonomy purposes. This is all handled through WebSockets.
            </div>

            <div className='subtitle'>Backend Server Code</div>
            <div className='section-description'>
                The backend code for the server was used to process the images since the raspberry pi was not powerful enough to do that and accept commands as well. In the python code, three separate threads were running to allow for different things to be happening at once.

                The first thread would read images sent from the robot&apos;s Raspberry Pi through gstreamer, put them into a queue for processing, and then send the image to the website using websockets where the backend of the website would be reading these images as input.

                The second thread would read from the queue of images from the first thread. When an image is in the queue the thread will take it, use OpenCV to detect where the lines are that should be followed by the robot, add a command to the command queue (if the robot should be moving autonomously) and then send the image to the website using gstreamer as well.

                Finally, the last thread is for commands being sent to the robot. This thread reads in commands from the website and relays them to the robot. This thread will also, when in autonomous mode, read from the commands queue and send these commands to the robot as well. The commands are sent and received using WebSockets.

                {/* TODO: Input a picture for the flow of commands */}
                OpenCV was used to read the input from the Raspberry Pi Camera stream. The image from the camera was placed into a queue for processing.
            </div>

            <div className='subtitle'>Server Configuration</div>
            <div className='section-description'>
                The website was hosted using NGINX. The robot would connect to the school WiFi, and using WireGuard, would send and receive messages through a VPN between its on board Raspberry Pi and my server running WireGuard. If the robot were to send a command, it would be received by NGINX through its configuration file.
            </div>

            <div className='subtitle'>Line Tracking</div>
            <div className='section-description'>
                One requirement of the project was to make the robot partially autonomous. We planned on using the camera for the feedback loop. Essentially, a certain color of tape would be placed in parallel lines in front of the robot and the robot could move forward, automatically adjusting its heading so that it stays between the lines.
            </div>

            <div className='subtitle'>Robot Features</div>
            <div className='section-description'>
                The robot was capable of numerous different movements. Using the website, the user could move its arm through each joint, they could move the robot forward and back, they could turn the robot left and right, they could move the camera up and down, and finally they could open and close the claw.
            </div>
        </>
    )
}

export default SeniorDesign