import React from 'react'

const HomeServer = () => {
    return (
        <>
            <div className='subtitle'>Specs</div>
            <div className='section-description'>
                The server is running on a mini PC with 32 GB of RAM, 1 TB of storage, and a Ryzen 7 5700U CPU. The OS for the mini PC is Proxmox, which is a hypervisor that allows for multiple virtual machines to be run on the same hardware.

                There is an additional 500 GB external hard drive for backing up the server so that if the server were to fail, the data would not be lost. The server also acts as my router, so it has two ethernet ports; one for the WAN and one for the LAN. These ports can support up to 1 GBps of data transfer.
            </div>

            <div className='subtitle'>Services Running</div>
            <div className='section-description'>
                Description of each of the services on the website. In each subsection, put in the amount of resources reserved to each system.
                There are 2 virtual machines and 5 containers running on the server:

                <h6 style={{paddingTop: '1rem'}}>Virtual Machines</h6> 
                <ul>
                    <li>OPNsense</li>
                    <li>HomeAssistant</li>
                </ul>
                <h6>Containers</h6> 
                <ul>
                    <li>Websites</li>
                    <li>Cloud</li>
                    <li>Nginx</li>
                    <li>ROS1</li>
                    <li>ROS2</li>
                </ul>

                The OPNsense virtual machine is used as a home router, daisy chaining off the apartment&apos;s router. The home assistant virtual machine is used to control the smart lights located around the apartment.

                The websites container hosts a number of personal websites for me through docker compose. These include the personal website, the robot website, capstone website, a website for a hackathon, and more. The cloud container is used to host a NextCloud instance for personal cloud storage and file sharing. The Nginx container is used as a reverse proxy for everything hosted locally on the server. This includes a configuration file which specifies things like proxied ports, SSL certificates, and proxy configurations. Finally, the ROS1 and ROS2 containers are used to practice and test robotic code in a deprecated ROS1 environment or the newer ROS2 environment.
            </div>

            <div className='subtitle'>Websites</div>
            <div className='section-description'>
                The Docker instance running on the website container contains a massive number of self-hosed websites. These are all the websites currently hosted in this container:
                <ul style={{paddingTop: '1rem'}}>
                    <li>Personal Portfolio Website</li>
                    <li>Personal Robot Website and Backend</li>
                    <li>Capstone Website</li>
                    <li>Hackathon Website</li>
                    <li>Bookstack and a DB for Bookstack</li>
                    <li>PgAdmin</li>
                    <li>MQTT Broker Server</li>
                    <li>Portainer</li>
                </ul>

                Some of these sites are only hosted locally without public facing DNS records for security such as the MQTT broker and PgAdmin. The MQTT broker is used for messaging from my personal robot to the server. This controls whether the robot is on or not. PgAdmin is also used for the robot to store the user data.

                The bookstack website is used to store all my notes and documentation for all my projects. This is hosted on the server so that I can access it from anywhere. The portainer website is used to manage all the docker containers on the server.
            </div>

            <div className='subtitle'>OPNsense</div>
            <div className='section-description'>
                The router of my home that hosts WireGuard and uses networking.
                OPNsense has been the most useful tool for me in terms of networking. It allows me to set up a VPN server on my server and connect to it from anywhere in the world. This is useful for when I am out and about and need to access my server. It also allows me to set up a firewall on my server to block any unwanted traffic.

                I have a WAN cable coming from the apartment&apos;s router into the server, and a outgoing LAN cable going to a network switch which connects to an access point granting WiFi to all the devices in the apartment. The server acts as a DHCP server for the apartment as well.
            </div>
        </>
    )
}

export default HomeServer