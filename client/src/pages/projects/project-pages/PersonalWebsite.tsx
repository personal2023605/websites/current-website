import React from 'react'
import skillsCarousel from '../markdown-assets/personal-website/skills-carousel.png'

const SeniorDesign = () => {
    return (
        <>
            <div className='subtitle'>Summary</div>
            <div className='section-description'>
                This website is programmed with TypeScript and React. It is hosted on a Docker container on my personal server and served with Nginx. The DNS being used is Cloudflare which also provides SSL for the website. There is a linter being used to ensure that the code is clean and readable and that it conforms to a certain desired format. I have also implemented CI/CD pipelines with GitLab actions to ensure that the code is tested and built before being deployed. If the build fails, it will not be deployed. This should ensure the website is rarely down and that the code is always working.
            </div>
            
            <div className='subtitle'>Organization</div>
            <div className='section-description'>
                The organization of the website is formatted in a way that is easy to understand and navigate. Each page of the website is a separate component that is imported into the main app component. Within each page, if there are special components these are placed in a components folder to be shared by other pages if they are needed.
            </div>

            <div className='subtitle'>Software</div>
            <div className='section-description'>
                As mentioned in the summary, the website is programmed with TypeScript and React. It also uses Styled Components for CSS styling and framer motion for animations. It utilizes Docker Compose for deployment and Nginx for serving the website. A VPN is used to securely connect GitLab to the server for deployment. The website is also linted with ESLint and formatted with Prettier. GitLab actions are used for CI/CD pipelines to ensure the code is tested and built before deployment. Cloudflare is used for DNS and SSL. 
            </div>

            <div className='subtitle'>Struggles</div>
            <div className='section-description'>
                Throughout the development of the website, there were a few struggles that I encountered. One of the biggest struggles was getting the website to deploy correctly. I had to learn how to use Docker and Nginx to host the website, but hosting locally proved to have a large learning curve. I also had to determine the best was to deploy the website. I eventually determined I wanted to use GitLab pipelines for deployment, which meant I needed to learn how to manage the security of GitLab variables since I would need to utilize keys for deployment. Eventually, I was able to use keys to allow GitLab to SSH into my server and deploy the website without human intervention. 

                For programming the website, a major difficulty was making the window dynamic. I wanted the website to be able to be viewed on any screen size and still look impressive. I studied different methods of doing this but honestly found none that made the process seamless. I eventually settled on listening to page resize events and changing a CSS variable based on the width of the screen. This was not the most elegant solution, but it functioned as desired and was a custom solution I could be proud of developing.

                A specific component on the website that was difficult to develop was the skills carousel. I wanted the carousel to loop indefinitely and show each skill I desired to boast. Although there were libraries that made this easier, the design was less than idea so I decided to program a component myself. Although this component was not difficult by itself, making it dynamic and responsive was a challenge. I eventually made the component work as desired using a framer motion animation and was happy with the result.

                <div className='description-img-container'>
                    <img style={{width: '60%', height: '60%'}} src={skillsCarousel}></img>
                    <div className='img-caption'>Website custom skill carousel</div>
                </div>
            </div>

            <div className='subtitle'>Turned Down Software</div>
            <div className='section-description'>
                There were a few software solutions that I wanted to use but ultimately decided against. 

                {/* TODO: Describe the technologies that were turned down */}
            </div>
        </>
    )
}

export default SeniorDesign