import { COLORS } from 'utils/Constants'
import ImageWithLoad from 'components/ImageWithLoad'
import { isMobileDevice } from 'utils/WindowHooks'
import ProjectList from './ProjectList'
import React from 'react'
import ReadMoreButton from 'components/ReadMoreButton'
import styled from 'styled-components'
import { useNavigate } from 'react-router-dom'

class Styles {
    static ProjectGrid = styled.div`
        display: grid;
        row-gap: 4vw;
        width: 90%;
        margin-bottom: 4vw;
        
        &.desktop {
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: 20vw 20vw 20vw;
            column-gap: 4vw;
        }

        &.mobile {
            grid-template-columns: 1fr;
            grid-template-rows: repeat(5, 30vh);
        }
    `

    static Project = styled.div`
        display: grid;
        grid-template-rows: 20% auto auto;
        grid-template-columns: 40% auto;
        background-color: ${COLORS.CARD_BACKGROUND};
        border-radius: 20px;
        gap: 10px;
        padding: 10px;
        
        & .image-container {
            display: flex;
            grid-row: 1 / 4;
            border-radius: 10px;
            border: 1px solid ${COLORS.BTN_BG};
            overflow: hidden;
            justify-content: center;
            align-items: center;
        }

        & .description-container {
            text-align: justify;
            overflow: auto;
        }
            
        & .project-title {
            text-align: center;
            align-items: center;
            width: 100%;
        }
            
        & .project-title-container {
            border-bottom: 1px solid gray;
            display: flex;
            align-items: center;
        }

        & .button-container {
            display: flex;
            justify-content: center;
            align-items: flex-end;
        }
    `
}

const Projects = () => {
    const isMobile = isMobileDevice()
    const navigate = useNavigate()

    const createProjectComponents = () => {
        return ProjectList.map((project, index) => {
            return (
                <Styles.Project key={index}>
                    <div className='image-container'>
                        <ImageWithLoad src={isMobile ? project.mobileImg : project.desktopImg} alt={project.title} />
                    </div>
                    <div className='project-title-container'>
                        <div className='cardTitle project-title'>{project.title}</div>
                    </div>
                    <div className='description-container'>
                        <div className='text project-description'>{project.description}</div>
                    </div>
                    <div className='button-container'>
                        <ReadMoreButton size={isMobile ? 3 : 1.5} className={'click-for-more'} text={'Read more!'} onClick={() => {navigate(project.link)}}/>
                    </div>
                </Styles.Project>
            )
        })
    }

    return (
        <div className='page'>
            <p className='title'>Projects</p>
            <Styles.ProjectGrid className={isMobile ? 'mobile' : 'desktop'}>
                {createProjectComponents()}
            </Styles.ProjectGrid>
        </div>
    )
}

export default Projects