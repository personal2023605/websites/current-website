import capstoneDesktop from './markdown-assets/capstone-portrait.jpg'
import homeServerDesktop from './markdown-assets/home-server.png'
import jplDesktop from './markdown-assets/jpl-portrait.jpg'
import jplMobile from './markdown-assets/jpl-landscape.jpg'
import personalRobot from './markdown-assets/personal-robot.jpg'
import { PROJECT_PAGES } from 'utils/Constants'
import websiteDesktop from './markdown-assets/personal-website.png'

export default [
    {
        desktopImg: jplDesktop,
        mobileImg: jplMobile,
        title: 'Senior Design Project',
        link: PROJECT_PAGES[0].path,
        description: `
            Reconstructed a model mars rover designed by JPL. Used a stereo camera to create a 3D model of the environment.
            The rover was controlled by ROS.
        `
    },
    {
        desktopImg: capstoneDesktop,
        mobileImg: capstoneDesktop,
        title: 'Capstone Project',
        link: PROJECT_PAGES[1].path,
        description: `
            Developed a web application that gives the user the ability control a robot from anywhere in the world.
            The site is equipped with controls and a camera to see where the robot is going.
        `
    },
    {
        desktopImg: websiteDesktop,
        mobileImg: websiteDesktop,
        title: 'Personal Website',
        link: PROJECT_PAGES[2].path,
        description: `
            Created a personal website to showcase my projects and experience.
            The site is built with React in TypeScript and is hosted using Docker containers on my home server.
        `
    },
    {
        desktopImg: homeServerDesktop,
        mobileImg: homeServerDesktop,
        title: 'Home Server',
        link: PROJECT_PAGES[3].path,
        description: `
            Setup a home server to host my personal website and other services.
            The server is running Proxmox.
        `
    },
    {
        desktopImg: personalRobot,
        mobileImg: personalRobot,
        title: 'Personal Robot',
        link: PROJECT_PAGES[4].path,
        description: `
            Create a robot that can be controlled from a public website. The robot uses
            ROS for control and peripherals, and has a postgreSQL DB for user information.
        `
    },
]