import Capstone from './project-pages/Capstone'
import HomeServer from './project-pages/HomeServer'
import PersonalRobot from './project-pages/PersonalRobot'
import PersonalWebsite from './project-pages/PersonalWebsite'
import React from 'react'
import SeniorDesign from './project-pages/SeniorDesign'
import styled from 'styled-components'

import { COLORS, PROJECT_PAGES, STYLES } from 'utils/Constants'

class Styles {
    static Container = styled.div`
        display: flex;
        flex-direction: column;

        & .md-container {
            margin: 0 5% 5% 5%;
            padding: 5%;
            background-color: ${COLORS.CARD_BACKGROUND};
            border-radius: 10px;
            box-shadow: 0 0 5px 1px #000000;
        }

        ${STYLES.CARD_TITLE('.subtitle')}

        & .project-section {
            display: flex;
            flex-direction: column;
        }

        & .project-container {
            display: flex;
            flex-direction: column;
            row-gap: 10px;
        }

        & .section-description {
            padding-bottom: 2rem;
        }

        & .description-img-container {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding-top: 1rem;
            padding-bottom: 1rem;
        }

        & .img-caption {
            color: gray;
            font-size: calc(var(--vh) * .01);
        }

        & a {
            text-decoration: underline;
        }
    `
}

const ProjectPage = (props: { name: string }) => {
    let projectComponent: JSX.Element = <></>
    const title = props.name

    switch (props.name.toLowerCase()) {
        case PROJECT_PAGES[0].name: projectComponent = <SeniorDesign/>; break
        case PROJECT_PAGES[1].name: projectComponent = <Capstone/>; break
        case PROJECT_PAGES[2].name: projectComponent = <PersonalWebsite/>; break
        case PROJECT_PAGES[3].name: projectComponent = <HomeServer/>; break
        case PROJECT_PAGES[4].name: projectComponent = <PersonalRobot/>; break
        default: break
    }
    
    return (
        <Styles.Container>
            {/* TODO: Put download for paper somewhere */}
            <p className='title'>{title}</p>
            <div className='md-container'>
                {projectComponent}
            </div>
        </Styles.Container>
    )
}

export default ProjectPage