import angular from '../assets/logos/angular.svg'
import aws from '../assets/logos/aws.svg'
import docker from '../assets/logos/docker.svg'
import helm from '../assets/logos/helm.svg'
import { isMobileDevice } from 'utils/WindowHooks'
import javascript from '../assets/logos/javascript.svg'
import kubernetes from '../assets/logos/kubernetes.svg'
import { motion } from 'framer-motion'
import nginx from '../assets/logos/nginx.svg'
import cucumber from '../assets/logos/cucumber.svg'
import jasmine from '../assets/logos/jasmine.svg'
import python from '../assets/logos/python.svg'
import react from '../assets/logos/react.svg'
import styled from 'styled-components'
import typescript from '../assets/logos/typescript.svg'
import redis from '../assets/logos/redis.svg'
import { v4 as uuid } from 'uuid'

import React, {ReactElement} from 'react'

type Skill = {
    title: string,
    icon: ReactElement
}

const SkillList: Skill[] = [
    { title: 'TypeScript', icon: <img src={typescript}/> },
    { title: 'React', icon: <img src={react}/> },
    { title: 'Kubernetes', icon: <img src={kubernetes}/> },
    { title: 'AWS', icon: <img src={aws}/> },
    { title: 'Helm', icon: <img src={helm}/> },
    { title: 'Docker', icon: <img src={docker}/> },
    { title: 'Angular', icon: <img src={angular}/> },
    { title: 'javaScript', icon: <img src={javascript}/> },
    { title: 'Python', icon: <img src={python}/> },
    { title: 'Nginx', icon: <img src={nginx}/> },
    { title: 'Cucumber', icon: <img src={cucumber}/> },
    { title: 'Jasmine', icon: <img src={jasmine}/> },
    { title: 'Redis', icon: <img src={redis}/> }
]

class Styles {
    static SkillsContainer = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        gap: 2vw;

        & .carousel-container {
            gap: 3%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & .carousel-alignment {
            display: flex;
            align-items: center;
        }

        & .carousel-element {
            display: grid;
            grid-template-rows: min(8vw, 100px) 3vw;
            grid-template-columns: min(8vw, 100px);
            height: 100%;
            gap: 1vw;
        }

        & .cardTitle {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & .image-container > img {
            width: 100%;
            max-height: 100%;
        }

        & .image-container {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & .carousel-element > p {
            text-align: center;
        }

        & .imageAlignmentContainer {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    `

    static MobileSkillsContainer = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;

        & * {
            margin: 0;
        }

        & .carousel-container {
            gap: 3%;
            flex-grow: 1;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & .carousel-alignment {
            height: 100%;
            display: flex;
            align-items: center;
        }

        & .carousel-element {
            display: grid;
            grid-template-rows: 10vw 3vw;
            grid-template-columns: 18vw;
            height: 100%;
            gap: 4vw;
        }

        & .image-container > img {
            width: 100%;
            max-height: 100%;
        }

        & .image-container {
            width: 80%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        & .carousel-element > p {
            text-align: center;
        }

        & .imageAlignmentContainer {
            width: 100%;
            min-height: 60%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    `
}

const SkillCarousel = () => {
    const [updatedList, setUpdatedList] = React.useState(SkillList)
    const isMobile = isMobileDevice()

    const reorderList = () => {
        // @ts-ignore
        SkillList.push(SkillList.shift())
        setUpdatedList(new Array(...SkillList))
    }

    return (
        <motion.div
            key={uuid()}
            className={'carousel-container'}
            id={'skill-carousel-container'}
            initial={{ x: 0 }}
            animate={{ x: isMobile ? '-23.7vw' : '-129px'}}
            transition={{ ease: 'linear', duration: 3, repeatType: 'loop'}}
            onAnimationComplete={reorderList}>
            {updatedList.map((skill, index) => {
                return (
                    <div key={index} className={'carousel-element'}>
                        <div className='imageAlignmentContainer'>
                            <div className='image-container'>
                                {skill.icon}
                            </div>
                        </div>
                        <p>{skill.title}</p>
                    </div>
                )
            })}
        </motion.div>
    )
}

const Skills = () => {
    const isMobile = isMobileDevice()
    
    if (isMobile) {
        return (
            <Styles.MobileSkillsContainer>
                <div className='cardTitle'>Skills</div>
                <div className='carousel-alignment'>
                    {SkillCarousel()}
                </div>
            </Styles.MobileSkillsContainer>
        )
    }
    return (
        <Styles.SkillsContainer>
            <div className='cardTitle'>Skills</div>
            <div className='carousel-alignment'>
                {SkillCarousel()}
            </div>
        </Styles.SkillsContainer>
    )
}

export default Skills