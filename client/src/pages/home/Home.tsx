import { COLORS } from 'utils/Constants'
import CurrentProject from './CurrentProject'
import { isMobileDevice } from 'utils/WindowHooks'
import JobDescription from './JobDescription'
import Profile from './Profile'
import React from 'react'
import Skills from './Skills'
import Socials from './Socials'
import StartTyping from './StartTyping'
import styled from 'styled-components'
import YearsOfExperience from './YearsOfExperience'

class Styles {
    static Container = styled.div`
        align-self: center;

        & > .grid-container {
            height: 100vh;
            width: min(100vw, 1200px);
            display: grid;
            grid-auto-rows: 1fr;
            grid-auto-columns: 1fr;
            gap: 2vh;
            padding: 2vh;
        }

        & > .grid-container.mobile {
            margin-top: 5%;
            padding-bottom: 5%;
            grid-template-rows: repeat(3, 15vh) 25vh repeat(1, 15vh);
        }

        & > .grid-container > .moduleCard {
            height: 100%;
            width: 100%;
            background-color: ${COLORS.CARD_BACKGROUND};
            border-radius: 25px;
            padding: 2vh;
            box-shadow: rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px;
            overflow: hidden;
        }
    `
}

const Home = () => {
    const isMobile = isMobileDevice()

    if (isMobile) {
        return (
            <>
                <Styles.Container>
                    <div className='grid-container mobile'>
                        <div className='moduleCard' style={{gridRow: '1 / 1'}}><Profile/></div>
                        <div className='moduleCard' style={{gridRow: '2 / 2'}}><JobDescription/></div>
                        <div className='moduleCard' style={{gridRow: '3 / 3'}}><YearsOfExperience/></div>
                        <div className='moduleCard' style={{gridRow: '4 / 4'}}><Skills/></div>
                        {/* <div className='moduleCard' style={{gridRow: '5 / 5'}}>Latest works</div> */}
                        <div className='moduleCard' style={{gridRow: '5 / 5'}}><Socials/></div>
                    </div>
                </Styles.Container>
            </>
        )
    }

    return (
        <>
            <Styles.Container>
                <div className='grid-container desktop'>
                    <div className='moduleCard' style={{gridArea: '1 / 1 / 4 / 2'}}><Profile/></div>
                    <div className='moduleCard' style={{gridArea: '4 / 1 / 4 / 4'}}><JobDescription/></div>
                    <div style={{gridArea: '3 / 2 / 4 / 4'}}><StartTyping/></div>
                    <div className='moduleCard' style={{gridArea: '3 / 4 / 6 / 5'}}><YearsOfExperience/></div>
                    <div className='moduleCard' style={{gridArea: '5 / 2 / 5 / 4'}}><Socials/></div>
                    <div className='moduleCard' style={{gridArea: '1 / 2 / 3 / 5'}}><Skills/></div>
                    <div id='current-project-card' className='moduleCard' style={{gridArea: '5 / 1 / 6 / 1'}}><CurrentProject/></div>
                </div>
            </Styles.Container>
        </>
    )
}

export default Home