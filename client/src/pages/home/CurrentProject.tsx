import $ from 'jquery'
import { COLORS } from 'utils/Constants'
import ProjectList from 'pages/projects/ProjectList'
import React from 'react'

import styled from 'styled-components'

const [project] = ProjectList

class Styles {
    static CurrentProjectContainer = styled.div`
        padding: 0;
        
        & .image-container {
            display: flex;
            border-radius: 10px;
            border: 1px solid ${COLORS.BTN_BG};
            overflow: hidden;
            justify-content: center;
            align-items: center;
        }
    `
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const setCardBackground = (entered: boolean) => {
    $('#current-project-card')
        .css('background-image', entered ? `url(${project.desktopImg})` : 'none')
}

const CurrentProject = () => {   
    $('#current-project-card')
        .on('mouseenter', () => setCardBackground(true))
        .on('mouseleave', () => setCardBackground(false))

    return (
        <Styles.CurrentProjectContainer>
            {/* TODO: Hover for image, maybe hover -> peel animation to see image */}
            {/* TODO: Make into button */}
            <div>See what I&apos;m working on!</div>
        </Styles.CurrentProjectContainer>
    )
}

export default CurrentProject