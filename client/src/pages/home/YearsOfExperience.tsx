import { isMobileDevice } from 'utils/WindowHooks'
import React from 'react'
import styled from 'styled-components'

class Styles {
    static Container = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;

        & > * {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 100%;
            gap: 2%;
        }

        & .number {
            font-size: 5vw;
        }

        & .caption {
            text-align: center;
        }
    `

    static MobileContainer = styled.div`
        height: 100%;
        display: flex;
        flex-direction: row;
        justify-content: space-evenly;

        & > * {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            width: 100%;
            gap: 2%;
        }

        & .subtitle {
            font-size: 2vw;
        }

        & .number {
            font-size: 5vw;
        }

        & .caption {
            text-align: center;
        }
    `
}

const YearsOfExperience = () => {
    const isMobile = isMobileDevice()
    
    const yearsOfExperienceContent = () => {
        return (
            <>
                <div>
                    <div className='number'>2</div>
                    <div className='caption'>Years experience</div>
                </div>
                <div>
                    <div className='number'>10</div>
                    <div className='caption'>Total Projects</div>
                </div>
                <div>
                    <div className='number'>5</div>
                    <div className='caption'>Websites Designed</div>
                </div>
            </>
        )
    }

    if (isMobile) {
        return (
            <Styles.MobileContainer>
                {yearsOfExperienceContent()}
            </Styles.MobileContainer>
        )
    }
    return (
        <Styles.Container>
            {yearsOfExperienceContent()}
        </Styles.Container>
    )
}

export default YearsOfExperience