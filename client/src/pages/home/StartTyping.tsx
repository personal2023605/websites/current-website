import { isMobileDevice } from 'utils/WindowHooks'
import React from 'react'
import styled from 'styled-components'
import { TypeAnimation } from 'react-type-animation'
import { useOverlay } from 'Router'

import { AnimatePresence, motion } from 'framer-motion'

class Styles {
    static StartTypingText = styled.div`
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;

        & > #websiteInstructions.mobile { height: 0; }

        & > #websiteInstructions > div {
            color: gray;
            font-size: min(3.2vw, 50px);
            display: 'inline-block'
        }

    `
}

const StartTyping = () => {
    const isMobile = isMobileDevice()
    const showOverlay = useOverlay()

    return (
        <Styles.StartTypingText>
            <div id='websiteInstructions' className={isMobile ? 'mobile' : 'desktop'}>
                {/* The text telling the user to search (animation for fade out) */}
                <AnimatePresence mode='wait'>
                    {(!showOverlay && !isMobile) && <motion.div
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }} >
                        {showOverlay ? <></> : <TypeAnimation sequence={ ['Start typing to explore...' ]} wrapper='div' speed={ 50 } />}
                    </motion.div>}
                </AnimatePresence>
            </div>
        </Styles.StartTypingText>
    )
}

export default StartTyping