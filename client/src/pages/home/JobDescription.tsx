import { isMobileDevice } from 'utils/WindowHooks'
import React from 'react'
import styled from 'styled-components'

class Styles {
    // TODO: Need to change font sizes to update here
    static JobDescriptionContainer = styled.div`
        height: 100%;
        overflow: auto;
        border-radius: 10px;
    `

    static MobileJobDescContainer = styled.div`
        height: 100%;
        overflow: auto;
        border-radius: 10px;

        & .description {
            text-align: justify;
        }
    `
}

const JobDescription = () => {
    const isMobile = isMobileDevice()
    
    const jobDescriptionContent = () => {
        return (
            <>
                {/* <div className='cardTitle'>Software Engineer</div> */}
                <div className='description'>
                    I am currently employed at Lockheed Martin as a full stack developer.
                    We develop with LitHTML for front end, and JavaScript and Java for backend.
                    We use Kubernetes clusters to host the services and AWS for developing with larger environments.
                    I have extensive experience with these softwares as well as Cucumber and Jasmine for testing, 
                    Redis for caching, and using SQL and NoSQL databases.
                </div>
            </>
        )
    }

    if (isMobile) {
        return (
            <Styles.MobileJobDescContainer>
                {jobDescriptionContent()}
            </Styles.MobileJobDescContainer>
        )
    }
    return (
        <Styles.JobDescriptionContainer>
            {jobDescriptionContent()}
        </Styles.JobDescriptionContainer>
    )
}

export default JobDescription