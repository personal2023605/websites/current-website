import { isMobileDevice } from 'utils/WindowHooks'
import styled from 'styled-components'

import { AiOutlineGithub, AiOutlineInstagram, AiOutlineLinkedin, AiOutlineMail } from 'react-icons/ai'
import React, { ReactElement } from 'react'

class Styles {
    static SocialsContainer = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;

        & .cardTitle {
            text-align: center;
        }

        & .socials {
            width: 100%;
            display: flex;
            justify-content: space-evenly;
            align-items: center;
        }
    `

    static MobileSocialsContainer = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;

        & .socials {
            height: 100%;
            width: 100%;
            display: flex;
            justify-content: space-evenly;
            align-items: center;
        }
    `
}

const iconButton = (props: {icon: ReactElement, link: string}) => {
    return (
        <a href={props.link}>
            {props.icon}
        </a>
    )
}

const Socials = () => {
    const isMobile = isMobileDevice()

    const socialsContent = () => {
        return (
            <>
                <div className='socials'>
                    {iconButton({icon: <AiOutlineGithub size={50} />, link: 'https://gitlab.com/ryanhodge240'})}
                    {iconButton({icon: <AiOutlineLinkedin size={50} />, link: 'https://www.linkedin.com/in/ryan-hodge-56479b1b6/'})}
                    {iconButton({icon: <AiOutlineInstagram size={50} />, link: 'https://www.instagram.com/ryanhodge240?igsh=d2M2NGJodG5kNmF2&utm_source=qr'})}
                    {/* TODO: Make it so this copies the email and add toast notification */}
                    {iconButton({icon: <AiOutlineMail size={50} />, link: 'mailto:ryanhodge240@gmail.com'})}
                </div>
                <div className='cardTitle'>Socials</div>
            </>
        )
    }

    if (isMobile) {
        return (
            <Styles.MobileSocialsContainer>
                {socialsContent()}
            </Styles.MobileSocialsContainer>
        )
    }
    return (
        <Styles.SocialsContainer>
            {socialsContent()}
        </Styles.SocialsContainer>
    )
}

export default Socials