import headshot from '../assets/headshot2.jpg'
import { isMobileDevice } from 'utils/WindowHooks'
import PlainTextButton from 'components/PlainTextButton'
import React from 'react'
import ReadMoreButton from 'components/ReadMoreButton'
import styled from 'styled-components'
import { useNavigate } from 'react-router-dom'

class Styles {
    static ProfileContainer = styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        gap: min(5vw, 70px);

        & > .img-container {
            width: 70%;
            margin-top: 5%;
        }

        & > .img-container > img {
            object-fit: cover;
            width: 100%;
            border-radius: 100%;
            aspect-ratio: 1 / 1;
        }

        & #profileButtonContainer {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            row-gap: 3vh;
            width: 60%;
        }

        & .profile-plain-text-button {
            width: 90%;
        }
    `

    static MobileProfileContainer = styled.div`
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        height: 100%;		

        & > .img-container {
            width: 40%;
        }

        & > .img-container > img {
            object-fit: cover;
            width: 60%;
            border-radius: 100%;
            aspect-ratio: 1 / 1;
        }

        & #profileButtonContainer {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            row-gap: 2vh;
        }

        & .profile-plain-text-button {
            width: 90%;
        }
    `
}

const Profile = () => {
    const isMobile = isMobileDevice()
    const navigate = useNavigate()

    const profileContent = () => {
        return (
            <>
                <div className='img-container'>
                    <img src={headshot} alt='headshot' />
                </div>
                <div id='profileButtonContainer'>
                    <ReadMoreButton className={'profile-button'} text={'Resume'} onClick={() => {navigate('/resume')}}/>
                    <PlainTextButton className={'profile-plain-text-button'} text={'Projects'} onClick={() => {navigate('/projects')}}/>
                </div>
            </>
        )
    }
    
    if (isMobile) {
        return (
            <Styles.MobileProfileContainer>
                {profileContent()}
            </Styles.MobileProfileContainer>
        )
    }
    return (
        <Styles.ProfileContainer>
            {profileContent()}
        </Styles.ProfileContainer>
    )
}

export default Profile