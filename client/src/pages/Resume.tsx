import Arrow from './assets/curved-arrow.png'
import ImageWithLoad from 'components/ImageWithLoad'
import { isMobileDevice } from 'utils/WindowHooks'
import ResumeDocument from './assets/resume.pdf'
import ResumeImage from './assets/resume.jpg'
import Styles from './ResumeStyles'

import { AnimatePresence, motion } from 'framer-motion'
import React, { useState } from 'react'

const Resume = () => {
    const [showDownloadText, setShowDownloadText] = useState(false)
    const isMobile = isMobileDevice()

    return (
        <Styles.Container className='page'>
            <h2 className='title'>Resume</h2>
            
            {isMobile && <div className='mobile-text'>
                <p>Click resume to download</p>
                <img id='arrow-icon' src={Arrow} alt='arrow' />
            </div>}
            <Styles.DocumentContainer>
                <a href={ResumeDocument}
                    className={isMobile ? 'mobile' : 'desktop'}
                    download={'ryan-hodge-resume'}
                    onMouseEnter={() => setShowDownloadText(true)}
                    onMouseLeave={() => setShowDownloadText(false)}>

                    {/* <img src={ResumeImage} alt='resume' /> */}
                    {/* TODO: Get the spinner working here */}
                    <ImageWithLoad src={ResumeImage} alt={'Resume'} />
                    <AnimatePresence mode='wait'>
                        {(showDownloadText && !isMobile) && <motion.div className='click-text-container'
                            key='testing'
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            exit={{ opacity: 0 }} >
                            <div className='first click-text'>Click to download</div>
                            <div className='second click-text'>Click to download</div>
                        </motion.div>}
                    </AnimatePresence>
                </a>
            </Styles.DocumentContainer>
        </Styles.Container>
    )
}

export default Resume