import { COLORS } from 'utils/Constants'
import styled from 'styled-components'

export default class ResumeStyles {
    static Container = styled.div`
        text-align: center;
        display: flex;
        flex-direction: column;

        & > .mobile-text {
            display: flex;
            justify-content: right;
            width: 90%;
            align-self: center;
            align-items: center;
            margin-bottom: 1vw;
        }

        & > .mobile-text > img {
            position: relative;
            top: 2.3vw;
            width: 5vw;
        }
    `
    
    static DocumentContainer = styled.div`
        display: flex;
        justify-content: center;
        min-width: 50%;

        & > a:hover {
            background-color: black;
        }

        & > a {
            width: 80%;
            max-width: 1000px;
            margin-bottom: 5%;
            
            border-radius: 10px;
        }

        & > a .click-text.first {
            top: 70%;
        }

        & > a .click-text.second {
            top: 30%;
        }

        & > a .click-text {
            position: relative;
            height: 0;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 3vw;
            color: black;
            font-weight: bold;
        }

        & > a .click-text-container {
            height: 100%;
            position: relative;
            top: -100%;
        }

        & > a > * > img {
            width: 100%;
            border-radius: 10px;
            box-shadow: 10px 10px 20px 5px ${COLORS.TRANSPARENT_BACKGROUND};
            transition: opacity 0.5s;
        }

        & > a.desktop:hover > * > img {
            opacity: 0.7;
        }
    `
}
