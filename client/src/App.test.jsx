import Footer from 'components/Footer'
import React from 'react'

import { expect, test } from '@jest/globals'
import { render, screen } from '@testing-library/react'


test('renders learn react link', () => {
    render(<Footer />)
    const linkElement = screen.getByText(/Made with React/i)
    expect(linkElement).toBeInTheDocument()
})
