import GlobalStyles from './utils/OverallStyling'
import React from 'react'
import Router from './Router'
import { RouterProvider } from 'react-router-dom'

class App extends React.Component {
    render(): React.ReactNode {
        return (
            <div className='app'>
                <GlobalStyles/>
                <RouterProvider router={Router}/>
            </div>
        )
    }
}

export default App
