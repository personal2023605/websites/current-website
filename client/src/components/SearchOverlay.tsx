import { PAGES } from 'utils/Constants'
import PropTypes from 'prop-types'
import { publish } from 'utils/Events'
import Styles from './SearchOverlayStyles'
import { useNavigate } from 'react-router-dom'

import { AnimatePresence, motion } from 'framer-motion'
import React, { ChangeEvent, useEffect, useState } from 'react'

const SearchOverlay = ({visible}: {visible: boolean}) => {

    const navigate = useNavigate()
    const searchableItems = PAGES
    const [searchInput, _setSearchInput] = useState('') // Used to set the value of the search bar (text in the search bar)
    const [searchResults, _setSearchResults] = useState<{index: number, name: string, path: string, subpath?: boolean}[]>(searchableItems) // Makes the results of the search from the user and displays them
    const [suggestion, _setSuggestion] = useState('') // Makes the results of the search from the user and displays them

    // On un-rendering, set these values
    useEffect(() => {
        if (!visible) {
            _setSearchInput('')
            _setSuggestion('')
        }
    }, [visible])

    const _toggleVisibility = () => {
        publish('closeOverlay')
        _setSearchInput('')
        _setSuggestion('')
    }

    /**
     * Escape controls getting out of the overlay
     * Arrows navigate results of search
     */
    const _keyPressHandler = (e: KeyboardEvent) => {
        if (e.altKey || e.ctrlKey || e.metaKey) return
        if (e.key === 'Escape') {
            _toggleVisibility()
            return
        }
        if (e.key === 'Enter') {
            const items = searchableItems.map(item => item.name.replace(/ /g, ''))
            if (items.includes(searchInput.replace(/ /g, ''))) {
                navigate(searchableItems[items.indexOf(searchInput.replace(/ /g, ''))].path)
                _toggleVisibility()
                return
            }
            _setSearchInput(suggestion)
        }
        else if (e.key === 'Tab') {
            e.preventDefault()
            _setSearchInput(suggestion)
        }
    }

    // Add key listener
    useEffect(() => {
        document.addEventListener('keydown', _keyPressHandler)
        return () => {
            document.removeEventListener('keydown', _keyPressHandler)
        }
    })

    // When typing, set the search input and results
    const _handleSearchChange = (e: ChangeEvent<HTMLInputElement>) => {
        const search = e.target.value
        _setSearchInput(search)
        if (search.length > 0) {
            const searchOutcome = searchableItems.map((item) => {
                return { ...item, index: item.name.indexOf(search.toLowerCase()), name: item.name }
            })
            _setSearchResults(searchOutcome.filter(item => item.index !== -1))

            let firstSuggestion = searchOutcome.filter(item => item.index === 0)[0]?.name ?? ''
            if (firstSuggestion.indexOf(search.toLowerCase()) === 0) {
                firstSuggestion = search + firstSuggestion.slice(search.length)
                _setSuggestion(firstSuggestion)
            }
            else {
                _setSuggestion('')
            }
        }
        else {
            _setSearchResults(searchableItems)
            _setSuggestion('')
        }
    }

    // Display the results of the search bar, sorted by quality of match
    const _displaySearchResults = () => {
        // TODO: Add path for sub-pages
        let sortedResults = searchInput.length === 0 ? searchableItems : searchResults
        sortedResults = sortedResults.sort((a, b) => a.index < b.index ? -1 : 1)
        return sortedResults.map((item, i) => {
            // @ts-ignore
            return (<button className={`result ${item.subpath ? 'subpath' : ''}`} id='navigation-link' key={i} onClick={() => {
                _toggleVisibility()
                navigate(item.path)
            }}>{item.name}</button>)
        })
    }

    // Bring the search bar back into focus
    const _refocusSearchBar = () => {
        document.getElementById('mainSearchBar')?.focus()
    }

    return (
        <AnimatePresence mode='wait'>
            {(visible) && <motion.div
                style={{ zIndex: 1 }}
                key='animateSearchOverlay'
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}>
                <Styles.Container onClick={process.env.REACT_APP_ENV === 'development' ? _refocusSearchBar : () => {}}>
                    <Styles.SearchBarContainer>
                        <input
                            onBlur={() => {process.env.REACT_APP_ENV !== 'development' ? _toggleVisibility() : null}}
                            className='title'
                            autoFocus
                            id='mainSearchBar'
                            type='text'
                            placeholder='Type to search'
                            onChange={_handleSearchChange}
                            value={searchInput}
                            autoComplete='off' />
                        <input
                            id='search-bar2'
                            className='title'
                            type='text'
                            value={suggestion}
                            autoComplete='off'
                            readOnly />
                        <Styles.ResultsContainer id='resultsContainer'>
                            {_displaySearchResults()}
                        </Styles.ResultsContainer>
                    </Styles.SearchBarContainer>
                </Styles.Container>
            </motion.div>}
        </AnimatePresence>
    )
}

SearchOverlay.propTypes = {
    setVisibility: PropTypes.func,
    visible: PropTypes.bool,
    firstKeyStroke: PropTypes.bool
}

export default SearchOverlay