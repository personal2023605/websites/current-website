import { COLORS } from 'utils/Constants'
import Logo from 'assets/travel-earth-circle.png'
import React from 'react'
import styled from 'styled-components'

class Styles {
    static FooterContainer = styled.footer`
        position: fixed;
        top: 94vh;
        height: 6vh;
        width: 100%;
        background-color: ${COLORS.PRIMARY};
        display: flex;
        justify-content: space-between;
        align-items: center;
        box-shadow: 0px -1px 10px 2px black;
        border-top: 2px solid black;

        & > * {
            padding-left: 5%;
            padding-right: 5%;
        }

        & > img {
            height: 80%;
        }
    `
}

const Footer = (props: {id: string}) => {
    return (
        <Styles.FooterContainer id={props.id}>
            <img src={Logo} alt="Logo" />
            <span>Made with React</span>
        </Styles.FooterContainer>
    )
}

export default Footer