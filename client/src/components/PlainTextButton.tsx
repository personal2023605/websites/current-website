import { COLORS } from 'utils/Constants'
import { MaterialSymbol } from 'react-material-symbols'
import React from 'react'
import styled from 'styled-components'

import { motion, useAnimationControls } from 'framer-motion'

class Styles {
    static Button = styled.div`
        color: ${COLORS.BTN_BG};
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        justify-content: space-between;
        transition: color 0.5s;

        &:hover {
            color: ${COLORS.BTN_BG_HOVER};
            cursor: pointer;
            transition: color 0.5s;
        }

        & .arrow-container {
            display: flex;
            align-items: center;
        }

        & .label {
            font-size: 14px;
        }
    `
}

const ReadMoreButton = (props: {className: string, text: string, onClick: () => void}) => {
    const controls = useAnimationControls()

    const fadeIn = () => {
        controls.set({ x: 0 })
        controls.start({ opacity: 1, x: 5 })
    }
    
    const fadeOut = () => {
        controls.start({ opacity: 0, x: 0 })
    }

    return (
        <Styles.Button className={props.className} onMouseEnter={fadeIn} onMouseLeave={fadeOut} onClick={props.onClick}>
            <p className='label' style={{margin: 0}}>{props.text}</p>
            <motion.div className='arrow-container' initial={{ x: 0, opacity: 0 }} animate={controls}>
                <MaterialSymbol id='arrow' size={20} icon="trending_flat" />
            </motion.div>
            <MaterialSymbol size={20} icon="person_book" />
        </Styles.Button>
    )
}

export default ReadMoreButton