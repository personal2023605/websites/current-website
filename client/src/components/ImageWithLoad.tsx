import ClipLoader from 'react-spinners/ClipLoader'
import React from 'react'
import styled from 'styled-components'

class Styles {
    static Container = styled.div`
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    `
}

const ImageWithLoad = (props: {src: string, alt: string, className?: string}) => {

    const [loading, setLoading] = React.useState(true)

    return (
        <Styles.Container>
            <img src={props.src}
                style={{
                    display: loading ? 'none' : 'block',
                    minHeight: '100%',
                    minWidth: '100%',
                    objectFit: 'cover'
                }}
                alt={props.alt}
                onLoad={() => setLoading(false)} />
            <ClipLoader
                color={'white'}
                loading={loading}
                aria-label='Ring Loader'
            />
        </Styles.Container>
    )
}

export default ImageWithLoad