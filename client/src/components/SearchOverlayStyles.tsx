import { COLORS } from 'utils/Constants'
import styled from 'styled-components'

export default class SearchOverlayStyles {
    static Container = styled.div`
        position: absolute;
        top: 0;
        height: 100vh;
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        background-color: ${COLORS.TRANSPARENT_BACKGROUND};
    `

    static SearchBarContainer = styled.div`
        width: min(80%, 600px);
        margin-top: 20vh;
        position: relative;

        & input {
            z-index: 2;
            width: 100%;
            overflow: visible;
            background-color: transparent;
            outline: none;
            border: none;
            border-bottom: 1px solid white;
            color: ${COLORS.TEXT};;
            position: absolute;
            padding: 0.5rem 0;
            font-size: max(4vw, 50px);
        }

        & #search-bar2 {
            z-index: 1;
            color: ${COLORS.DARK_DISABLED_TEXT};
            cursor: none;
        }
    `

    static ResultsContainer = styled.div`
        position: absolute;
        top: max(10vw, 100px);
        display: flex;
        flex-direction: column;
        text-transform: capitalize;
        width: 100%;
        height: 40vh;
        overflow: auto;

        & .result {
            margin: 0;
            letter-spacing: 1px;
            font-size: max(2vw, 30px);
            background-color: transparent;
            text-transform: capitalize;
            padding-left: 0.75rem;
        }

        & .subpath {
            font-size: max(1vw, 20px);
            padding-left: 1.5rem;
        }

        & > #navigation-link {
            text-align: left;
            color: ${COLORS.DARK_TEXT};
            border: 2px solid transparent;
        }

        & > #navigation-link:hover {
            cursor: pointer;
            background-color: #0000003d;
            border: 2px solid #00000090;
            color: ${COLORS.DARK_HOVER_TEXT};
        }
    `
}
