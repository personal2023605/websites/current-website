import { Button as BSButton } from 'react-bootstrap'
import { COLORS } from 'utils/Constants'
import React from 'react'
import styled from 'styled-components'

class Styles {
    static Button = styled(BSButton)`
        background-color: ${COLORS.BTN_BG};
        color: ${COLORS.BTN_TEXT};
        border: 1px solid ${COLORS.BTN_BORDER};

        &:hover {
            background-color: ${COLORS.BTN_TEXT};
            color: ${COLORS.BTN_HOVER_TEXT};
            border: 1px solid ${COLORS.BTN_BORDER};
        }
    `
}

const Button: React.FC<{text: string, href: string, className?: string}> = (props) => {
    return (
        // @ts-ignore
        <Styles.Button
            className={props.className}
            href={props.href}>
            {props.text}
        </Styles.Button>
    )
}

export default Button