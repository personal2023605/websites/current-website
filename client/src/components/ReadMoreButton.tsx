import { COLORS } from 'utils/Constants'
import React from 'react'
import styled from 'styled-components'

class Styles {
    static Container = styled.div`
        --button-color:  ${COLORS.BTN_BG};
        --text-color: ${COLORS.BTN_TEXT};
        width: 100%;
        
        & .css-buttons-io-button:hover {
            --button-color:  ${COLORS.BTN_BG_HOVER};
            transition: background-color 0.5s;
        }
            
        & .css-buttons-io-button {
            width: 100%;
            font-size: 14px;
            background: var(--button-color);
            color: var(--text-color);
            font-family: inherit;
            padding: 0.35em;
            padding-left: 1.2em;
            font-weight: 500;
            border-radius: 0.9em;
            border: none;
            letter-spacing: 0.05em;
            display: flex;
            align-items: center;
            box-shadow: inset 0 0 1.6em -0.6em var(--button-color);;
            overflow: hidden;
            position: relative;
            height: 2.8em;
            padding-right: 3.3em;
            cursor: pointer;
        }

        & .css-buttons-io-button .icon {
            background: var(--text-color);
            margin-left: 1em;
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 2.2em;
            width: 2.2em;
            border-radius: 0.7em;
            box-shadow: 0.1em 0.1em 0.6em 0.2em var(--button-color);;
            right: 0.3em;
            transition: all 0.3s;
        }

        & .css-buttons-io-button:hover .icon {
            width: calc(100% - 0.6em);
        }

        & .css-buttons-io-button .icon svg {
            width: 1.1em;
            transition: transform 0.3s;
            color: var(--button-color);;
        }

        & .css-buttons-io-button:hover .icon svg {
            transform: translateX(0.1em);
        }

        & .css-buttons-io-button:active .icon {
            transform: scale(0.95);
        }
    `
}

const ReadMoreButton = (props: {className: string, text: string, onClick: () => void, size?: number}) => {
    return (
        <Styles.Container className={props.className}>
            <button className='css-buttons-io-button label' onClick={props.onClick} style={{fontSize: `min(${props.size ?? 10}vw, 14px)`}}>
                {props.text}
                <div className='icon'>
                    <svg
                        height='24'
                        width='24'
                        viewBox='0 0 24 24'
                        xmlns='http://www.w3.org/2000/svg'>
                        <path d='M0 0h24v24H0z' fill='none'></path>
                        <path
                            d='M16.172 11l-5.364-5.364 1.414-1.414L20 12l-7.778 7.778-1.414-1.414L16.172 13H4v-2z'
                            fill='currentColor'/>
                    </svg>
                </div>
            </button>
        </Styles.Container>
    )
}

export default ReadMoreButton