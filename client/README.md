# Client Code
[Inspiration](https://caferati.me)
[More inspiration](https://preview.themeforest.net/item/bentox-personal-portfolio-html-templates/full_screen_preview/50513423?_ga=2.268853237.52944253.1712024148-329426049.1712024148)

## Buttons to use
- https://uiverse.io/kennyotsu/witty-bullfrog-54
- https://uiverse.io/vikas7754/evil-mule-52

## TODO
- [ ] Add contact icons 
  - [ ] import { AiOutlineLinkedin } from "react-icons/ai";
  - [ ] import { AiOutlineInstagram } from "react-icons/ai";
  - [ ] import { AiOutlineGithub } from "react-icons/ai";
  - [ ] import { AiOutlineMail } from "react-icons/ai";
- [ ] Add skills icons
  - [ ] import { TbBrandTypescript } from "react-icons/tb";
  - [ ] import { SiKubernetes } from "react-icons/si";
  - [ ] import { FaAws } from "react-icons/fa";
  - [ ] import { FaReact } from "react-icons/fa";
  - [ ] import { SiHelm } from "react-icons/si";
  - [ ] import { FaDocker } from "react-icons/fa";
- [ ] Fix linter (details in eslint.js)