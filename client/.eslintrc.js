/**
 * TODO:
 * Things I want the linter to do
 * 1) Spaces after curly brackets
 * 2) Import order
 * 3) Line length
 * 4) Make sure class names are hyphenated
 */
module.exports = {
    'env': {
        'browser': true,
        'es2021': true,
        'jest': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:react/recommended'
    ],
    'overrides': [{
        'env': { 'node': true },
        'files': [ '.eslintrc.{js,cjs}' ],
        'parserOptions': { 'sourceType': 'script' }
    }],
    'parser': '@typescript-eslint/parser',
    'parserOptions': {
        'ecmaVersion': 'latest',
        'sourceType': 'module'
    },
    'plugins': [
        '@typescript-eslint',
        'react'
    ],
    'ignorePatterns': ['**/*.html'],
    'settings': {
        'react': { 'version': 'detect' }
    },
    'rules': {
        'block-scoped-var': 'warn',
        'camelcase': 'warn',
        'complexity': ['warn', 8],
        'consistent-return': 'error',
        'default-case': 'error',
        'default-case-last': 'error',
        'default-param-last': 'error',
        'eqeqeq': 'error',
        'id-length': ['warn', { 'min': 1, 'max': 25 }],
        'no-duplicate-imports': 'error',
        'no-else-return': 'error',
        'no-lone-blocks': 'error',
        'no-lonely-if': 'error',
        'no-nested-ternary': 'error',
        'no-self-compare': 'error',
        'no-unneeded-ternary': 'error',
        'no-use-before-define': 'error',
        'no-var': 'error',
        'prefer-arrow-callback': 'warn',
        'prefer-const': 'error',
        'prefer-destructuring': 'warn',
        'prefer-template': 'warn',
        'quotes': ['error', 'single'],
        'require-await': 'error',
        'semi': ['error', 'never'],
        'sort-vars': 'error',
        'yoda': 'error',
        '@typescript-eslint/ban-ts-comment': 'off'
    }
}
