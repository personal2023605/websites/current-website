FROM nginx
EXPOSE 80
COPY build/* /usr/share/nginx/html
COPY ryanhodge-nginx.conf /etc/nginx/conf.d/default.conf