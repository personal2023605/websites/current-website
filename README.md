# Personal Website
[Tutorial Followed](https://www.youtube.com/watch?v=0aPLk2e2Z3g)
[Inspiration](https://caferati.me)
[More inspiration](https://preview.themeforest.net/item/bentox-personal-portfolio-html-templates/full_screen_preview/50513423?_ga=2.268853237.52944253.1712024148-329426049.1712024148)

## Client
Use the variable `process.env.REACT_APP_ENV`. This will be set to either `development` or `production`.
```sh
# Running in dev mode, makes small changes so development isn't as tricky
npm start

# Running in production mode, simulates how website would work in real world
npm run production
```

## Deploying
Automatic deployment is set up via merge requests in GitLab. To automatically deploy to the server, once the MR is approved add the `deploy` tag and rerun pipelines, this will kick off deployment.

Deployment will build the project, copy the files over to the server using scp, and finally docker will recognize that the files changed and it will deploy the changes, making them visible on the live website.

# Resources Used
- []()
